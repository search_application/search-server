package SearchServer;


import SearchServer.ElasticSearch.ElasticSearchInterface;
import SearchServer.Service.IndexInterface;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by rahul on 25/01/16.
 */
@RestController
public class SuggestController {

    @CrossOrigin
    @RequestMapping(value = "/suggestBody")
    public List<String> suggestBody(@RequestParam("entry") String entry){

        List<String> words  = Arrays.asList(entry.split(" ")).stream().filter(p -> p.length() > 0).collect(Collectors.toList());

        String last_word = words.get(words.size() - 1);
        String remaining_words = words.subList(0, words.size() - 1).stream().collect(Collectors.joining(" "));

        IndexInterface indexInterface = new ElasticSearchInterface();
        List<String> completion_candidates = indexInterface.getMessageBodySuggestion(entry);

        System.out.println(completion_candidates);

        return completion_candidates
                .stream()
                .map(str -> str.split(" "))
                .map(arr -> arr[words.size() - 1])
                .map(p -> remaining_words.equals("")? p : remaining_words + " " + p)
                .collect(Collectors.toList());

    }

    @CrossOrigin
    @RequestMapping(value = "/suggestContact")
    public List<String> suggestContact(@RequestParam("entry") String entry){


        IndexInterface indexInterface = new ElasticSearchInterface();
        return indexInterface.getContactNameSuggestion(entry);

    }

}
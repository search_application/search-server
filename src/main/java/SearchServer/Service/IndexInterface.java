package SearchServer.Service;




import SearchServer.Model.Query.QueryWrapper;
import SearchServer.Model.Result.DocumentResultEntity;
import SearchServer.Model.Result.ImageResultEntity;
import SearchServer.Model.Result.MessageResultEntity;
import SearchServer.Model.Result.ResultsWrapper;

import java.util.List;

/**
 *
 * @author rahul
 */
public interface IndexInterface {
    
    public ResultsWrapper<MessageResultEntity> getMessageText(QueryWrapper queryWrapper);
    
    public ResultsWrapper<DocumentResultEntity> queryDocument(QueryWrapper queryWrapper);

    public ResultsWrapper<ImageResultEntity> queryImageByExample(QueryWrapper queryWrapper);

    public ResultsWrapper<ImageResultEntity> queryImageByTag(QueryWrapper queryWrapper);

    public List<String> getMessageBodySuggestion(String bodyEntry);

    public List<String> getContactNameSuggestion(String nameEntry);
}

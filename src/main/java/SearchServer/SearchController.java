package SearchServer;


import SearchServer.Model.Query.QueryWrapper;
import SearchServer.Model.Result.*;
import SearchServer.Security.ResultFilterer;
import SearchServer.Service.IndexInterface;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by rahul on 23/01/16.
 */

@RestController
public class SearchController {



    @Resource(name = "elasticSearchBean")
    IndexInterface indexInterface;


    @RequestMapping(value = "/queryMessage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResultsWrapper<MessageResultEntity> queryMessage(@RequestBody QueryWrapper queryWrapper, Authentication authentication){

        ResultsWrapper<MessageResultEntity> results = indexInterface.getMessageText(queryWrapper);

        String username = authentication.getName();
        List<GrantedAuthority> roles = authentication.getAuthorities()
                                .stream()
                                .collect(Collectors.toList());


        Logger.getLogger("SearchServer").log(Level.INFO, roles.toString());



        //figure out role of incoming user
        //in future figure out the list of accepted users someone can view via a separate service
        boolean admin = roles
                .stream()
                .map(Object::toString)
                .map(role -> role.substring(16, role.length() - 1))
                .anyMatch(role -> ResultFilterer.ADMIN.getAuthority().equals(role));

        if(admin)
            Logger.getLogger("ResultsFilterer").log(Level.INFO, username + " identified as ADMIN");
        else
            Logger.getLogger("ResultsFilterer").log(Level.INFO, username + " identified as USER");


        List<MessageResultEntity> filteredResultEntities = results.getResultList().stream().filter(result -> ResultFilterer.filter(username, admin, result)).collect(Collectors.toList());
        results.setResultList(filteredResultEntities);

        Logger.getLogger("Filter").log(Level.INFO, "Filtering for " + username + ":" + results.getResultList());
        return results;
    }




    @RequestMapping(value = "/queryFile", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResultsWrapper<DocumentResultEntity> queryDocument(@RequestBody QueryWrapper queryWrapper, Authentication authentication){

        ResultsWrapper<DocumentResultEntity> results = indexInterface.queryDocument(queryWrapper);

        String username = authentication.getName();
        List<GrantedAuthority> roles = authentication.getAuthorities()
                .stream()
                .collect(Collectors.toList());

        Logger.getLogger("SearchServer").log(Level.INFO, roles.toString());

        //figure out role of incoming user
        //in future figure out the list of accepted users someone can view via a separate service
        boolean admin = roles
                .stream()
                .map(Object::toString)
                .map(role -> role.substring(16, role.length() - 1))
                .anyMatch(role -> ResultFilterer.ADMIN.getAuthority().equals(role));

        if(admin)
            Logger.getLogger("ResultsFilterer").log(Level.INFO, username + " identified as ADMIN");
        else
            Logger.getLogger("ResultsFilterer").log(Level.INFO, username + " identified as USER");


        List<DocumentResultEntity> filteredResultEntities = results.getResultList().stream().filter(result -> ResultFilterer.filter(username, admin, result)).collect(Collectors.toList());
        results.setResultList(filteredResultEntities);

        Logger.getLogger("Filter").log(Level.INFO, "Filtering for " + username + ":" + results.getResultList());
        return results;
    }



    @RequestMapping(value = "/queryImage/example", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResultsWrapper<ImageResultEntity> queryImageByExample(@RequestBody QueryWrapper queryWrapper, Authentication authentication){

        ResultsWrapper<ImageResultEntity> results = indexInterface.queryImageByExample(queryWrapper);

        String username = authentication.getName();
        List<GrantedAuthority> roles = authentication.getAuthorities()
                .stream()
                .collect(Collectors.toList());

        Logger.getLogger("SearchServer").log(Level.INFO, roles.toString());

        //figure out role of incoming user
        //in future figure out the list of accepted users someone can view via a separate service
        boolean admin = roles
                .stream()
                .map(Object::toString)
                .map(role -> role.substring(16, role.length() - 1))
                .anyMatch(role -> ResultFilterer.ADMIN.getAuthority().equals(role));

        if(admin)
            Logger.getLogger("ResultsFilterer").log(Level.INFO, username + " identified as ADMIN");
        else
            Logger.getLogger("ResultsFilterer").log(Level.INFO, username + " identified as USER");


        List<ImageResultEntity> filteredResultEntities = results.getResultList().stream().filter(result -> ResultFilterer.filter(username, admin, result)).collect(Collectors.toList());
        results.setResultList(filteredResultEntities);

        Logger.getLogger("Filter").log(Level.INFO, "Filtering for " + username + ":" + results.getResultList());
        return results;
    }



    @RequestMapping(value = "/queryImage/tags", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResultsWrapper<ImageResultEntity> queryImageByTags(@RequestBody QueryWrapper queryWrapper, Authentication authentication){

        ResultsWrapper<ImageResultEntity> results = indexInterface.queryImageByTag(queryWrapper);

        String username = authentication.getName();
        List<GrantedAuthority> roles = authentication.getAuthorities()
                .stream()
                .collect(Collectors.toList());

        Logger.getLogger("SearchServer").log(Level.INFO, roles.toString());

        //figure out role of incoming user
        //in future figure out the list of accepted users someone can view via a separate service
        boolean admin = roles
                .stream()
                .map(Object::toString)
                .map(role -> role.substring(16, role.length() - 1))
                .anyMatch(role -> ResultFilterer.ADMIN.getAuthority().equals(role));

        if(admin)
            Logger.getLogger("ResultsFilterer").log(Level.INFO, username + " identified as ADMIN");
        else
            Logger.getLogger("ResultsFilterer").log(Level.INFO, username + " identified as USER");


        List<ImageResultEntity> filteredResultEntities =
                results.getResultList()
                        .stream()
                        .filter(result -> ResultFilterer.filter(username, admin, result))
                        .collect(Collectors.toList());

        results.setResultList(filteredResultEntities);

        Logger.getLogger("Filter").log(Level.INFO, "Filtering for " + username + ":" + results.getResultList());
        return results;
    }


}

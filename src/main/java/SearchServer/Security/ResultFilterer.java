package SearchServer.Security;

import SearchServer.Model.Result.ResultEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Created by rahul on 1/29/16.
 */
public class ResultFilterer {

    public static SimpleGrantedAuthority USER = new SimpleGrantedAuthority("USER");
    public static GrantedAuthority ADMIN = new SimpleGrantedAuthority("ADMIN");

    public static boolean filter(String username, boolean admin, ResultEntity resultEntity){

        //in future accept a list of approved usernames
        if(admin)
            return true;
        else{
            //see if username if present in atleast from or to feilds
            return resultEntity.getFrom().toLowerCase().equals(username.toLowerCase()) || resultEntity.getTo().toLowerCase().equals(username.toLowerCase());
        }
    }
}

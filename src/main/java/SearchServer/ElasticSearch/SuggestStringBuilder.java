/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SearchServer.ElasticSearch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 *
 * @author rahul
 */
public class SuggestStringBuilder {
    
    
    public static String buildBodySuggestString(String entry){
        
        ObjectMapper mapper = new ObjectMapper();
        JsonNodeFactory nodeFactory = mapper.getNodeFactory();
        
        
        ObjectNode fieldNode = nodeFactory.objectNode().put("field", "body_suggest");
        
        ObjectNode suggestionBodyNode = nodeFactory.objectNode();
        suggestionBodyNode.put("text", entry);
        suggestionBodyNode.set("completion", fieldNode);
        
        ObjectNode root = nodeFactory.objectNode();
        root.set("body_suggest", suggestionBodyNode);
        
        return root.toString();
        
    }
    
    
    public static String buildNameSuggestString(String entry){
        
        ObjectMapper mapper = new ObjectMapper();
        JsonNodeFactory nodeFactory = mapper.getNodeFactory();
        
        
        ObjectNode fieldNode = nodeFactory.objectNode().put("field", "name_suggest");
        
        ObjectNode suggestionBodyNode = nodeFactory.objectNode();
        suggestionBodyNode.put("text", entry);
        suggestionBodyNode.set("completion", fieldNode);
        
        ObjectNode root = nodeFactory.objectNode();
        root.set("name_suggest", suggestionBodyNode);
        
        return root.toString();
        
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SearchServer.ElasticSearch;

import SearchServer.Model.Result.*;
import SearchServer.Model.Query.QueryWrapper;
import SearchServer.Service.IndexInterface;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author rahul
 */

@Service("elasticSearchBean")
public class ElasticSearchInterface implements IndexInterface {

    public static String messageQueryURL = "http://localhost:9200/chat/message/_search";
    public static String fileQueryURL = "http://localhost:9200/uploads/_search";

    @Value("${fileServerURL}")
    public String fileServerURL;


    @Override
    public ResultsWrapper<MessageResultEntity> getMessageText(QueryWrapper wrapper) {

        ObjectMapper mapper = new ObjectMapper();

        List<MessageResultEntity> resultEntityList = new LinkedList<>();
        ResultsWrapper<MessageResultEntity> resultList = new ResultsWrapper<>();

        String url = messageQueryURL;

        String query = QueryStringBuilder.buildMessageQuery(wrapper);
        System.out.println(query);

        try {

            HttpResponse<String> res = Unirest.post(url).body(query).asString();
            Logger.getGlobal().log(Level.INFO, res.getBody());

            JsonNode root = mapper.readTree(res.getBody());

            resultList.setQuery(wrapper.bodyFeild);
            resultList.setSearchTime(root.get("took").toString());
            resultList.setResultNum(root.get("hits").get("total").toString());

            JsonNode jsonResultList = root.get("hits").get("hits");

            SimpleDateFormat queryDateFormatter = new SimpleDateFormat("dd/MM/yyyy:h:mm:a");
            SimpleDateFormat outputDateFormatter = new SimpleDateFormat("dd/MM/yyyy h:mm a");

            if (jsonResultList.isArray()) {
                for (JsonNode result : jsonResultList) {

                    JsonNode content = result.get("_source");

                    String date = outputDateFormatter.format(queryDateFormatter.parse(content.get("date").toString().replace("\"", "")));
                    MessageResultEntity resultEntity = new MessageResultEntity();

                    resultEntity.setDate(date);
                    resultEntity.setFrom(content.get("from").toString().replace("\"", ""));
                    resultEntity.setTo(content.get("to").toString().replace("\"", ""));
                    resultEntity.setMessageText(content.get("body").toString());


                    JsonNode highlights = result.get("highlight");


                    if (highlights != null) {
                        StringBuilder builder = new StringBuilder();
                        for (JsonNode snippet : highlights.get("body")) {
                            builder.append(snippet.asText() + ".....");
                            //fs.extractedContent += snippet.toString() + "...";
                        }

                        resultEntity.messageText = builder.toString();
                    }



                    resultEntityList.add(resultEntity);

                }

                resultList.setResultList(resultEntityList);
            }

        } catch (UnirestException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultList;
    }

    @Override
    public ResultsWrapper<DocumentResultEntity> queryDocument(QueryWrapper queryWrapper) {
        
        ObjectMapper mapper = new ObjectMapper();

        String url = fileQueryURL;
        String query = QueryStringBuilder.buildDocumentQuery(queryWrapper);

        System.out.println(query);

        ResultsWrapper<DocumentResultEntity> resultList = new ResultsWrapper<>();
        List<DocumentResultEntity> retrievedResults = new ArrayList<>();

        try {

            HttpResponse<String> res = Unirest.post(url).body(query).asString();

            JsonNode root = mapper.readTree(res.getBody());

            System.out.println("*****************************************************************");
            //System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root));
            Logger.getGlobal().log(Level.INFO, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root));

            resultList.setQuery(queryWrapper.extractedContentFeild);
            resultList.setSearchTime(root.get("took").toString());
            resultList.setResultNum(root.get("hits").get("total").toString());

            JsonNode jsonResultList = root.get("hits").get("hits");

            if (jsonResultList.isArray()) {
                for (JsonNode result : jsonResultList) {

                    JsonNode content = result.get("_source");

                    DocumentResultEntity resultEntity = new DocumentResultEntity();
                    resultEntity.fileId = content.get("fileId").asText();
                    resultEntity.filename = content.get("filename").asText();
                    resultEntity.from = content.get("from").asText();
                    resultEntity.to = content.get("to").asText();
                    resultEntity.url = fileServerURL + content.get("filename").asText();

                    JsonNode highlights = result.get("highlight");

                    System.out.println(resultEntity.url);

                    if (highlights != null) {
                        StringBuilder builder = new StringBuilder();
                        for (JsonNode snippet : highlights.get("extractedContent")) {
                            builder.append(snippet.asText() + ".....");
                            //fs.extractedContent += snippet.toString() + "...";
                        }

                        resultEntity.extractedContent = builder.toString();
                    }

                    retrievedResults.add(resultEntity);
                }
            }

            resultList.setResultList(retrievedResults);

        } catch (UnirestException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultList;

    }



    @Override
    public ResultsWrapper<ImageResultEntity> queryImageByExample(QueryWrapper queryWrapper) {

        ObjectMapper mapper = new ObjectMapper();

        String url = fileQueryURL;
        String query = QueryStringBuilder.buildImageQuery(queryWrapper);

        System.out.println(query);

        ResultsWrapper<ImageResultEntity> resultList = new ResultsWrapper<>();
        List<ImageResultEntity> retrievedResults = new ArrayList<>();

        try {

            HttpResponse<String> res = Unirest.post(url).body(query).asString();

            JsonNode root = mapper.readTree(res.getBody());

            System.out.println("*****************************************************************");
            Logger.getGlobal().log(Level.INFO, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root));

            resultList.setQuery(queryWrapper.filenameFeild);
            resultList.setSearchTime(root.get("took").toString());
            resultList.setResultNum(root.get("hits").get("total").toString());

            JsonNode jsonResultList = root.get("hits").get("hits");

            if (jsonResultList.isArray()) {
                for (JsonNode result : jsonResultList) {

                    JsonNode content = result.get("_source");

                    ImageResultEntity resultEntity = new ImageResultEntity();
                    resultEntity.fileId = content.get("fileId").asText();
                    resultEntity.filename = content.get("filename").asText();
                    resultEntity.from = content.get("from").asText();
                    resultEntity.to = content.get("to").asText();
                    resultEntity.url = fileServerURL + content.get("filename").asText();

                    System.out.println(resultEntity.url);
                    retrievedResults.add(resultEntity);
                }

            }

            resultList.setResultList(retrievedResults);

        } catch (UnirestException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultList;

    }

    @Override
    public ResultsWrapper<ImageResultEntity> queryImageByTag(QueryWrapper queryWrapper){

        ObjectMapper mapper = new ObjectMapper();

        String url = fileQueryURL;
        String query = QueryStringBuilder.buildImageByTagQuery(queryWrapper);

        System.out.println(query);

        ResultsWrapper<ImageResultEntity> resultList = new ResultsWrapper<>();
        List<ImageResultEntity> retrievedResults = new ArrayList<>();





        try {

            HttpResponse<String> res = Unirest.post(url).body(query).asString();

            JsonNode root = mapper.readTree(res.getBody());

            System.out.println("*****************************************************************");
            //System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root));
            Logger.getGlobal().log(Level.INFO, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root));

            resultList.setQuery(queryWrapper.extractedContentFeild);
            resultList.setSearchTime(root.get("took").toString());
            resultList.setResultNum(root.get("hits").get("total").toString());

            JsonNode jsonResultList = root.get("hits").get("hits");

            if (jsonResultList.isArray()) {
                for (JsonNode result : jsonResultList) {

                    JsonNode content = result.get("_source");

                    ImageResultEntity wrapper = new ImageResultEntity();
                    wrapper.fileId = content.get("fileId").asText();
                    wrapper.filename = content.get("filename").asText();
                    wrapper.from = content.get("from").asText();
                    wrapper.to = content.get("to").asText();
                    wrapper.CEDDField = content.get("cedd").asText();

                    wrapper.url = fileServerURL + content.get("filename").asText();
                    System.out.println(wrapper.url);

                    retrievedResults.add(wrapper);
                }
            }

            resultList.setResultList(retrievedResults);

        } catch (UnirestException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultList;
    }





    @Override
    public List<String> getMessageBodySuggestion(String bodyEntry) {

        ObjectMapper mapper = new ObjectMapper();

        String url = "http://localhost:9200/chat/_suggest";
        String query = SuggestStringBuilder.buildBodySuggestString(bodyEntry);

        System.out.println(query);
        HttpResponse<String> res;
        try {
            res = Unirest.post(url).body(query).asString();

            Logger.getGlobal().log(Level.INFO, res.getBody());
            JsonNode root = mapper.readTree(res.getBody());

            System.out.println("*****************************************************************");
            Logger.getGlobal().log(Level.INFO, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root));

            List<String> result_set = new ArrayList<>();

            JsonNode suggestionFieldList = root.get("body_suggest");
            if(suggestionFieldList.isArray()){
                for(JsonNode suggestionNode : suggestionFieldList){

                    JsonNode optionsList = suggestionNode.get("options");
                    if(optionsList.isArray()){
                        for(JsonNode option : optionsList){
                            result_set.add(option.get("text").asText());
                        }
                    }

                }
            }





            return result_set;


        } catch (UnirestException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }


    @Override
    public List<String> getContactNameSuggestion(String nameEntry) {

        ObjectMapper mapper = new ObjectMapper();

        String url = "http://localhost:9200/chat/_suggest";
        String query = SuggestStringBuilder.buildNameSuggestString(nameEntry);

        System.out.println(query);
        HttpResponse<String> res;
        try {
            res = Unirest.post(url).body(query).asString();

            Logger.getGlobal().log(Level.INFO, res.getBody());
            JsonNode root = mapper.readTree(res.getBody());

            System.out.println("*****************************************************************");
            //System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root));
            Logger.getGlobal().log(Level.INFO, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root));

            List<String> result_set = new ArrayList<>();

            JsonNode suggestionFieldList = root.get("name_suggest");
            if(suggestionFieldList.isArray()){
                for(JsonNode suggestionNode : suggestionFieldList){

                    JsonNode optionsList = suggestionNode.get("options");
                    if(optionsList.isArray()){
                        for(JsonNode option : optionsList){
                            result_set.add(option.get("text").asText());
                        }
                    }

                }
            }





            return result_set;


        } catch (UnirestException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ElasticSearchInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SearchServer.ElasticSearch;

import SearchServer.Model.Query.QueryWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;


/**
 *
 * @author rahul
 */
public class QueryStringBuilder {


    public static String buildMessageQuery(QueryWrapper queryWrapper){
        
        ObjectMapper mapper = new ObjectMapper();
        JsonNodeFactory nodeFactory = mapper.getNodeFactory();

        ObjectNode root = nodeFactory.objectNode();
        root.put("size", "100");
        root.set("query", nodeFactory.objectNode().set("filtered", nodeFactory.objectNode()));

        ObjectNode queryNode = (ObjectNode) root.with("query").with("filtered").set("query", nodeFactory.objectNode());
        ObjectNode filterNode = (ObjectNode) root.with("query").with("filtered").set("filter", nodeFactory.objectNode());


        //----------------------------------------------------------------------
        // Query Section
        if(queryWrapper.unionFromTo == false){
            ArrayNode mustList = nodeFactory.arrayNode();

            if(queryWrapper.fromFeild != null){
                ObjectNode matchNode = nodeFactory.objectNode();
                matchNode.set("match", nodeFactory.objectNode().set("from", nodeFactory.objectNode()));
                matchNode.with("match").with("from").put("query", queryWrapper.fromFeild);
                matchNode.with("match").with("from").put("operator", "or");
                mustList.add(matchNode);
            }

            if(queryWrapper.toFeild != null){
                ObjectNode matchNode = nodeFactory.objectNode();
                matchNode.set("match", nodeFactory.objectNode().set("to", nodeFactory.objectNode()));
                matchNode.with("match").with("to").put("query", queryWrapper.toFeild);
                matchNode.with("match").with("to").put("operator", "or");
                mustList.add(matchNode);
            }

            if(queryWrapper.bodyFeild != null){
                ObjectNode matchNode = nodeFactory.objectNode();
                matchNode.set("match", nodeFactory.objectNode().set("body", nodeFactory.objectNode()));
                matchNode.with("match").with("body").put("query", queryWrapper.bodyFeild);
                matchNode.with("match").with("body").put("fuzziness", "AUTO");
                matchNode.with("match").with("body").put("operator", "or");
                mustList.add(matchNode);
            }

            queryNode.with("query").set("bool", nodeFactory.objectNode().set("must", mustList));

        }else{

            ArrayNode mustList = nodeFactory.arrayNode();
            ArrayNode shouldList = nodeFactory.arrayNode();

            if(queryWrapper.fromFeild != null){
                ObjectNode matchNode = nodeFactory.objectNode();
                matchNode.set("match", nodeFactory.objectNode().set("from", nodeFactory.objectNode()));
                matchNode.with("match").with("from").put("query", queryWrapper.fromFeild);
                matchNode.with("match").with("from").put("operator", "or");
                shouldList.add(matchNode);
            }

            if(queryWrapper.toFeild != null){
                ObjectNode matchNode = nodeFactory.objectNode();
                matchNode.set("match", nodeFactory.objectNode().set("to", nodeFactory.objectNode()));
                matchNode.with("match").with("to").put("query", queryWrapper.toFeild);
                matchNode.with("match").with("to").put("operator", "or");
                shouldList.add(matchNode);
            }

            if(queryWrapper.bodyFeild != null){
                ObjectNode matchNode = nodeFactory.objectNode();
                matchNode.set("match", nodeFactory.objectNode().set("body", nodeFactory.objectNode()));
                matchNode.with("match").with("body").put("query", queryWrapper.bodyFeild);
                matchNode.with("match").with("body").put("fuzziness", "AUTO");
                matchNode.with("match").with("body").put("operator", "or");
                mustList.add(matchNode);
            }

            queryNode.with("query").set("bool", nodeFactory.objectNode().set("should", shouldList));
            queryNode.with("query").with("bool").set("must", mustList);
            queryNode.with("query").with("bool").put("minimum_should_match", 1);

        }
        //----------------------------------------------------------------------



        //----------------------------------------------------------------------
        // Filter Section

        if( queryWrapper.fromDateFeild != null || queryWrapper.toDateFeild != null){

            ObjectNode rangeNode = nodeFactory.objectNode();
            filterNode.with("filter").set("range", rangeNode.set("date", nodeFactory.objectNode()));

            if(queryWrapper.fromDateFeild != null)
                rangeNode.with("date").put("gte", queryWrapper.fromDateFeild);

            if(queryWrapper.toDateFeild != null)
                rangeNode.with("date").put("lte", queryWrapper.toDateFeild);
        }

        //----------------------------------------------------------------------

        ObjectNode highlightNode = (ObjectNode) root.set("highlight", nodeFactory.objectNode());
        highlightNode.with("highlight").set("fields", nodeFactory.objectNode().set("body", nodeFactory.objectNode().put("type", "plain")));


        return root.toString();
        
        
    }
    

    public static String buildDocumentQuery(QueryWrapper queryWrapper){

        ObjectMapper mapper = new ObjectMapper();
        JsonNodeFactory nodeFactory = mapper.getNodeFactory();

        ObjectNode root = nodeFactory.objectNode();
        root.set("query", nodeFactory.objectNode().set("filtered", nodeFactory.objectNode()));

        ObjectNode queryNode = (ObjectNode) root.with("query").with("filtered").set("query", nodeFactory.objectNode());
        ObjectNode filterNode = (ObjectNode) root.with("query").with("filtered").set("filter", nodeFactory.objectNode());

        //----------------------------------------------------------------------
        // Query Section

        ArrayNode mustList = nodeFactory.arrayNode();
        ArrayNode shouldList = nodeFactory.arrayNode();

        if(queryWrapper.fromFeild != null){
            ObjectNode matchNode = nodeFactory.objectNode();
            matchNode.set("match", nodeFactory.objectNode().set("from", nodeFactory.objectNode()));
            matchNode.with("match").with("from").put("query", queryWrapper.fromFeild);
            matchNode.with("match").with("from").put("operator", "or");
            mustList.add(matchNode);
        }

        if(queryWrapper.toFeild != null){
            ObjectNode matchNode = nodeFactory.objectNode();
            matchNode.set("match", nodeFactory.objectNode().set("to", nodeFactory.objectNode()));
            matchNode.with("match").with("to").put("query", queryWrapper.toFeild);
            matchNode.with("match").with("to").put("operator", "or");
            mustList.add(matchNode);
        }

        if(queryWrapper.extractedContentFeild != null){
            ObjectNode matchNode = nodeFactory.objectNode();
            matchNode.set("match", nodeFactory.objectNode().set("extractedContent", nodeFactory.objectNode()));
            matchNode.with("match").with("extractedContent").put("query", queryWrapper.extractedContentFeild);
            matchNode.with("match").with("extractedContent").put("operator", "or");
            mustList.add(matchNode);
        }

        if(queryWrapper.tags != null){
            ObjectNode matchNode = nodeFactory.objectNode();
            matchNode.set("fuzzy", nodeFactory.objectNode().put("tags", queryWrapper.tags));
            shouldList.add(matchNode);
        }


        queryNode.with("query").set("bool", nodeFactory.objectNode().set("should", shouldList));
        queryNode.with("query").with("bool").set("must", mustList);
        //queryNode.with("query").with("bool").put("minimum_should_match", 1);

        //----------------------------------------------------------------------



        //----------------------------------------------------------------------
        // Filter Section

        if( queryWrapper.mimeType != null){

            ObjectNode termNode = nodeFactory.objectNode();
            termNode.put("mimeType", queryWrapper.mimeType);
            filterNode.with("filter").set("term", termNode);

        }

        //----------------------------------------------------------------------


        ObjectNode highlightNode = (ObjectNode) root.set("highlight", nodeFactory.objectNode());
        highlightNode.with("highlight").set("fields", nodeFactory.objectNode().set("extractedContent", nodeFactory.objectNode().put("type", "plain")));

        return root.toString();
    }


    public static String buildImageQuery(QueryWrapper queryWrapper){

        ObjectMapper mapper = new ObjectMapper();
        JsonNodeFactory nodeFactory = mapper.getNodeFactory();

        ObjectNode root = nodeFactory.objectNode();
        root.set("query", nodeFactory.objectNode().set("filtered", nodeFactory.objectNode()));

        ObjectNode queryNode = (ObjectNode) root.with("query").with("filtered").set("query", nodeFactory.objectNode());
        ObjectNode filterNode = (ObjectNode) root.with("query").with("filtered").set("filter", nodeFactory.objectNode());

        //----------------------------------------------------------------------
        // Query Section

        ArrayNode mustList = nodeFactory.arrayNode();
        ArrayNode shouldList = nodeFactory.arrayNode();

        if(queryWrapper.fromFeild != null){
            ObjectNode matchNode = nodeFactory.objectNode();
            matchNode.set("match", nodeFactory.objectNode().set("from", nodeFactory.objectNode()));
            matchNode.with("match").with("from").put("query", queryWrapper.fromFeild);
            matchNode.with("match").with("from").put("operator", "or");
            mustList.add(matchNode);
        }

        if(queryWrapper.toFeild != null){
            ObjectNode matchNode = nodeFactory.objectNode();
            matchNode.set("match", nodeFactory.objectNode().set("to", nodeFactory.objectNode()));
            matchNode.with("match").with("to").put("query", queryWrapper.toFeild);
            matchNode.with("match").with("to").put("operator", "or");
            mustList.add(matchNode);
        }

        if(queryWrapper.CEDDField != null){
            ObjectNode matchNode = nodeFactory.objectNode();
            matchNode.set("fuzzy", nodeFactory.objectNode().put("cedd", queryWrapper.CEDDField));
            mustList.add(matchNode);
        }

        if(queryWrapper.tags != null){
            ObjectNode matchNode = nodeFactory.objectNode();
            matchNode.set("fuzzy", nodeFactory.objectNode().put("tags", queryWrapper.tags));
            shouldList.add(matchNode);
        }

        queryNode.with("query").set("bool", nodeFactory.objectNode().set("should", shouldList));
        queryNode.with("query").with("bool").set("must", mustList);

        //----------------------------------------------------------------------



        //----------------------------------------------------------------------
        // Filter Section

        if( queryWrapper.mimeType != null){

            ObjectNode termNode = nodeFactory.objectNode();
            termNode.put("mimeType", queryWrapper.mimeType);
            filterNode.with("filter").set("term", termNode);

        }

        //root.set("query", nodeFactory.objectNode().set("match", nodeFactory.objectNode()));
        //root.with("query").with("match").put("cedd", queryWrapper.CEDDField);

        return root.toString();
    }

    public static String buildImageByTagQuery(QueryWrapper queryWrapper){

        ObjectMapper mapper = new ObjectMapper();
        JsonNodeFactory nodeFactory = mapper.getNodeFactory();

        ObjectNode root = nodeFactory.objectNode();
        root.set("query", nodeFactory.objectNode().set("filtered", nodeFactory.objectNode()));

        ObjectNode queryNode = (ObjectNode) root.with("query").with("filtered").set("query", nodeFactory.objectNode());
        ObjectNode filterNode = (ObjectNode) root.with("query").with("filtered").set("filter", nodeFactory.objectNode());

        //----------------------------------------------------------------------
        // Query Section

        ArrayNode mustList = nodeFactory.arrayNode();

        if(queryWrapper.tags != null){
            ObjectNode matchNode = nodeFactory.objectNode();
            matchNode.set("match", nodeFactory.objectNode().put("tags", queryWrapper.tags));
            mustList.add(matchNode);
        }


        queryNode.with("query").set("bool", nodeFactory.objectNode().set("should", mustList));

        //----------------------------------------------------------------------



        //----------------------------------------------------------------------
        // Filter Section

        if( queryWrapper.mimeType != null){

            ObjectNode termNode = nodeFactory.objectNode();
            termNode.put("mimeType", queryWrapper.mimeType);
            filterNode.with("filter").set("regexp", termNode);

        }



        return root.toString();
    }
}

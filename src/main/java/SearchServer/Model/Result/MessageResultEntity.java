package SearchServer.Model.Result;


public class MessageResultEntity extends ResultEntity {

    public String messageText;

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
}
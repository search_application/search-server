package SearchServer.Model.Result;


/**
 * Created by rahul on 25/01/16.
 */
public class ImageResultEntity extends ResultEntity {


    public String fileId;
    public String filename;
    public String CEDDField;
    public String url;
    

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getCEDDField() {
        return CEDDField;
    }

    public void setCEDDField(String CEDDField) {
        this.CEDDField = CEDDField;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}

package SearchServer.Model.Query;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rahul on 25/01/16.
 */
public class DocumentQueryWrapper {

    //fields for files
    public String filenameField;
    public String mimeType;
    public String contentField;
    public String tags;


    @JsonCreator
    public DocumentQueryWrapper(@JsonProperty("filenameField") String filenameField, @JsonProperty("mimeType") String mimeType,
                                @JsonProperty("contentField") String contentField, @JsonProperty("tags") String tags) {

        this.filenameField = filenameField;
        this.mimeType = mimeType;
        this.contentField = contentField;
        this.tags = tags;
    }

    public String getFilenameField() {
        return filenameField;
    }

    public void setFilenameField(String filenameField) {
        this.filenameField = filenameField;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getContentField() {
        return contentField;
    }

    public void setContentField(String contentField) {
        this.contentField = contentField;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}

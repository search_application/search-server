package SearchServer.Model.Query;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rahul on 25/01/16.
 */
public class ImageQueryWrapper {

    //fields for image
    public String filenameField;
    public String mimeType;
    public String CEDDField;
    public String tags;

    @JsonCreator
    public ImageQueryWrapper(@JsonProperty("filenameField")String filenameField, @JsonProperty("mimeType")String mimeType,
                             @JsonProperty("CEDDDField")String CEDDField, @JsonProperty("tags")String tags) {

        this.filenameField = filenameField;
        this.mimeType = mimeType;
        this.CEDDField = CEDDField;
        this.tags = tags;
    }

    public String getFilenameField() {
        return filenameField;
    }

    public void setFilenameField(String filenameField) {
        this.filenameField = filenameField;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getCEDDField() {
        return CEDDField;
    }

    public void setCEDDField(String CEDDField) {
        this.CEDDField = CEDDField;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}

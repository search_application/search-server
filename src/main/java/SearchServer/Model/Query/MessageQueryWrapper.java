package SearchServer.Model.Query;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rahul on 23/01/16.
 */
public class MessageQueryWrapper {

    //feilds for message
    public String fromField;
    public String toField;
    public String bodyField;
    public String fromDateField;
    public String toDateField;


    @JsonCreator
    public MessageQueryWrapper(@JsonProperty("fromField")String fromField, @JsonProperty("toField")String toField,
                               @JsonProperty("bodyField")String bodyField, @JsonProperty("fromDateField")String fromDateField,
                               @JsonProperty("toDateField")String toDateField) {


        this.fromField = fromField;
        this.toField = toField;
        this.bodyField = bodyField;
        this.fromDateField = fromDateField;
        this.toDateField = toDateField;
    }


    public String getFromField() {
        return fromField;
    }

    public void setFromField(String fromField) {
        this.fromField = fromField;
    }

    public String getToField() {
        return toField;
    }

    public void setToField(String toField) {
        this.toField = toField;
    }

    public String getBodyField() {
        return bodyField;
    }

    public void setBodyField(String bodyField) {
        this.bodyField = bodyField;
    }

    public String getFromDateField() {
        return fromDateField;
    }

    public void setFromDateField(String fromDateField) {
        this.fromDateField = fromDateField;
    }

    public String getToDateField() {
        return toDateField;
    }

    public void setToDateField(String toDateField) {
        this.toDateField = toDateField;
    }
}
